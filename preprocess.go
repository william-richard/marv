package marv

import (
	"bufio"
	"bytes"
)

// Preprocess cleans up an input byte stream
// right now, it just splits it into non-empty lines,
// but in the future, it could do more complex sanity checking
// and cleaning
func Preprocess(dirty []byte) (clean []Line, err error) {
	scanner := bufio.NewScanner(bytes.NewBuffer(dirty))

	for scanner.Scan() {
		s := scanner.Text()
		if len(s) > 0 {
			clean = append(clean, Line{scanner.Text()})
		}
	}

	return
}
