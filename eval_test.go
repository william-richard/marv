package marv

import (
	"fmt"
	"reflect"
	"testing"
)

func TestExecuteStatements(t *testing.T) {
	cases := []struct {
		in   []Smt
		want Environment
	}{
		{
			[]Smt{Assign{ID{"a"}, IntExpr{"5"}}},
			Environment{map[ID]MValue{
				ID{"a"}: MValue{Int, "5"},
			}},
		},
		{
			[]Smt{Assign{ID{"seven"}, Plus{IntExpr{"4"}, IntExpr{"3"}}}},
			Environment{map[ID]MValue{
				ID{"seven"}: MValue{Int, "7"},
			}},
		},
		{
			[]Smt{
				Assign{ID{"three"}, IntExpr{"3"}},
				Assign{ID{"six"}, IntExpr{"6"}},
				Assign{ID{"nine"}, Plus{ID{"three"}, ID{"six"}}},
			},
			Environment{map[ID]MValue{
				ID{"three"}: MValue{Int, "3"},
				ID{"six"}:   MValue{Int, "6"},
				ID{"nine"}:  MValue{Int, "9"},
			}},
		},
		{
			[]Smt{Assign{ID{"s"}, StringExpr{"hello world"}}},
			Environment{map[ID]MValue{
				ID{"s"}: MValue{String, "hello world"},
			}},
		},
	}

	failCases := []struct {
		in   []Smt
		want error
	}{
		{
			[]Smt{Assign{ID{"mixedPlusType"}, Plus{IntExpr{"5"}, StringExpr{"5"}}}},
			fmt.Errorf("Types of values passed to plus are different! Got Int with value 5 and String with value 5"),
		},
	}

	for _, c := range cases {
		e := NewEnvironment()
		err := e.ExecuteStatements(c.in)
		if err != nil {
			t.Errorf("ExecuteStatements(%v) threw an err. %v", c.in, err.Error())
		}
		if !reflect.DeepEqual(*e, c.want) {
			t.Fatalf("ExecuteStatements(%v) == %v. Wanted %v", c.in, e, c.want)
		}
	}

	for _, c := range failCases {
		e := NewEnvironment()
		err := e.ExecuteStatements(c.in)
		if err == nil {
			t.Fatalf("Expected ExecuteStatements(%v) to return error %v, but it did not return any error", c.in, c.want)
		}
		if err.Error() != c.want.Error() {
			t.Fatalf("ExecuteStatements(%v) error message was '%s'. Expected it to be '%s'", c.in, err.Error(), c.want.Error())
		}
	}
}

func BenchmarkNewEnvironment(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewEnvironment()
	}
}

func BenchmarkEvalAssignInt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements([]Smt{Assign{ID{"a"}, IntExpr{"5"}}})
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}

func BenchmarkEvalAssignStr(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements([]Smt{Assign{ID{"a"}, StringExpr{"hello world"}}})
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}

func BenchmarkEvalPlusInt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements([]Smt{Assign{ID{"a"}, Plus{IntExpr{"5"}, IntExpr{"3"}}}})
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}

func BenchmarkEvalPlusStr(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements([]Smt{Assign{ID{"a"}, Plus{StringExpr{"hello "}, StringExpr{"world"}}}})
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}

func BenchmarkEvalPlusIntVars(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements(
			[]Smt{
				Assign{ID{"a"}, IntExpr{"5"}},
				Assign{ID{"b"}, IntExpr{"4"}},
				Assign{ID{"c"}, Plus{ID{"a"}, ID{"b"}}},
			},
		)
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}

func BenchmarkEvalPlusStrVars(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := NewEnvironment()
		err := e.ExecuteStatements(
			[]Smt{
				Assign{ID{"a"}, StringExpr{"hello "}},
				Assign{ID{"b"}, StringExpr{"world"}},
				Assign{ID{"c"}, Plus{ID{"a"}, ID{"b"}}},
			},
		)
		if err != nil {
			b.Errorf("Error while executing! %s", err.Error())
		}
	}
}
