package marv

import (
	"reflect"
	"testing"
)

func TestPreprocess(t *testing.T) {

	cases := []struct {
		in   []byte
		want []Line
	}{
		{[]byte("single line test"), []Line{Line{"single line test"}}},
		{[]byte("multiple line\ntest"), []Line{Line{"multiple line"}, Line{"test"}}},
	}

	for _, c := range cases {
		got, err := Preprocess(c.in)
		if err != nil {
			t.Errorf("Preprocess(%q) threw an error. %q", c.in, err.Error())
		}
		if !reflect.DeepEqual(got, c.want) {
			t.Fatalf("Preprocess(%q) == %q, wanted %q", c.in, got, c.want)
		}
	}

}

func BenchmarkPreprocess(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Preprocess([]byte("short two\nline string"))
	}
}
