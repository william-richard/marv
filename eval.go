package marv

import "fmt"

// Environment describes the state of the program as its running
// It maps variables to their evaluated values
type Environment struct {
	env map[ID]MValue
}

// NewEnvironment creates an empty environment
func NewEnvironment() *Environment {
	e := new(Environment)
	e.env = make(map[ID]MValue)
	return e
}

// PrintEnv is a debug function that prints the state of the environment
func (e Environment) PrintEnv() {
	fmt.Printf("%#v\n", e.env)
}

// Lookup returns the MValue assigned to the ID in the Environment
func (e Environment) Lookup(id ID) (val MValue, err error) {
	val, isset := e.env[id]
	if !isset {
		return val, fmt.Errorf("ID %#v is not set", id)
	}
	return val, nil
}

// Set assigns an ID to an MValue
// it is used to assign variables by statements
func (e Environment) Set(id ID, val MValue) (err error) {
	// TODO how to handle reassignment?
	curVal, alreadySet := e.env[id]
	if alreadySet {
		return fmt.Errorf("Cannot set id %#v to %#v.  It was already set to %#v", id, val, curVal)
	}
	e.env[id] = val
	return nil
}

// ExecuteStatements executes the statements on the environment
// it modifies the state of the environment, based on the effects
// of each statement
func (e Environment) ExecuteStatements(smts []Smt) (err error) {
	for _, smt := range smts {
		e, err = smt.Execute(e)
		if err != nil {
			return err
		}
	}
	return nil
}
