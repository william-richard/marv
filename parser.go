package marv

import (
	"fmt"
	"regexp"
	"strconv"
)

// ParseRule is one rule used to parse Marv code
type ParseRule struct {
	name  string
	regex *regexp.Regexp
}

// Parser is a struct containing rules needed to parse Marv code
type Parser struct {
	smtRules  []ParseRule
	exprRules []ParseRule
}

// NewParser returns a parser struct, with all the rules needed to parse Marv code
func NewParser() *Parser {
	p := new(Parser)
	id := regexp.MustCompile("([[:alnum:]]+)")
	litint := regexp.MustCompile("([[:digit:]]+)")
	litstr := regexp.MustCompile(`"(.*?)"`)
	expr := regexp.MustCompile(`(.*?)`)

	anchorRule := func(name string, pattern_parts []*regexp.Regexp) ParseRule {
		regexpStrig := "^"
		for _, pp := range pattern_parts {
			regexpStrig = regexpStrig + pp.String()
		}
		regexpStrig = regexpStrig + "$"
		reg := regexp.MustCompile(regexpStrig)
		return ParseRule{name, reg}
	}

	p.smtRules = []ParseRule{
		anchorRule("Assign", []*regexp.Regexp{id, regexp.MustCompile(" = "), expr}),
	}

	p.exprRules = []ParseRule{
		// THIS ORDERING MATTERS!
		// there are expressions that can match multiple rules
		// like ID and LitInt
		// The more specific rule (LitInt) must come first
		// otherwise the less specific rule (ID) will always be used
		// similarly, any expression that includes other expressions
		// (like Plus) should come before any literal expressions
		// (like LitInt and LitStr) so the more complex rules have a chance
		// to match first
		anchorRule("Plus", []*regexp.Regexp{expr, regexp.MustCompile(" \\+ "), expr}),
		anchorRule("LitInt", []*regexp.Regexp{litint}),
		anchorRule("LitStr", []*regexp.Regexp{litstr}),
		anchorRule("ID", []*regexp.Regexp{id}),
	}

	return p
}

// Line simply contains a line of code
type Line struct {
	content string
}

// Parse all the passed lines of code, returning the statements that
// are found
func (p Parser) Parse(lines []Line) (res []Smt, err error) {
	res = make([]Smt, 0)
	for _, line := range lines {
		smt, err := p.ParseSmt(line.content)
		if err != nil {
			return res, err
		}
		res = append(res, smt)
		if err != nil {
			return res, err
		}
	}
	return
}

// ParseSmt parses a line of code for statements
// It calls the other, specific statement parsing functions
// and returns the result of one that does not fail
func (p Parser) ParseSmt(s string) (Smt, error) {
	var res Smt
	for _, rule := range p.smtRules {
		if rule.regex.MatchString(s) {
			c := rule.regex.FindStringSubmatch(s)
			switch rule.name {
			case "Assign":
				return p.Assign(c)
			}
			return res, fmt.Errorf("Line '%s' did not match statement - matched '%s' instead", s, rule.name)
		}
	}
	return res, fmt.Errorf("Line '%s' did not match any rules!", s)
}

// Assign parses the given string, and tries to find an assignment
// statement
func (p Parser) Assign(cap []string) (Smt, error) {
	var res Smt
	id, err := p.ParseID(cap[1])
	if err != nil {
		return res, err
	}
	exp, err := p.ParseExp(cap[2])
	if err != nil {
		return res, err
	}
	return Assign{id, exp}, nil
}

// ParseID parses the given string, and tries to find an ID in it
func (p Parser) ParseID(s string) (ID, error) {
	var res ID
	exp, err := p.ParseExp(s)
	if err != nil {
		return res, err
	}
	switch potentialID := exp.(type) {
	case ID:
		return potentialID, nil
	}
	return res, fmt.Errorf("Parse error. Expected ID - got '%#v'", exp)
}

// ParseExp parses an expression
// it tries to call all other expression parsing functions,
// and returns the results of one that does not fail
func (p Parser) ParseExp(s string) (res Expr, err error) {
	for _, rule := range p.exprRules {
		if rule.regex.MatchString(s) {
			c := rule.regex.FindStringSubmatch(s)
			switch rule.name {
			case "ID":
				res, err = p.ID(c)
			case "LitInt":
				res, err = p.LitInt(c)
			case "LitStr":
				res, err = p.LitStr(c)
			case "Plus":
				res, err = p.Plus(c)
			}
			if res != nil {
				return res, nil
			}
		}
	}
	if err != nil {
		return nil, fmt.Errorf("String '%s' matched at least one  rule, but all rules threw an error.  Last matching rule threw: %s", s, err.Error())
	}
	return res, fmt.Errorf("String '%s' did not match any rules!", s)
}

// ID returns an id expression, if one can be found
func (p Parser) ID(cap []string) (Expr, error) {
	return ID{cap[1]}, nil
}

// LitInt returns a literal integer expression, if one can be found
func (p Parser) LitInt(cap []string) (Expr, error) {
	var res Expr
	_, err := strconv.Atoi(cap[1])
	if err != nil {
		return res, err
	}
	return IntExpr{cap[1]}, nil
}

// LitStr returns a Literal String expression if one can be found
func (p Parser) LitStr(cap []string) (res Expr, err error) {
	return StringExpr{cap[1]}, nil
}

// Plus will return a Plus expression, if it can find one in
// passed line of code.
func (p Parser) Plus(cap []string) (Expr, error) {
	var res Expr
	e1, err := p.ParseExp(cap[1])
	if err != nil {
		return res, err
	}
	e2, err := p.ParseExp(cap[2])
	if err != nil {
		return res, err
	}
	return Plus{e1, e2}, nil
}
