package marv

// Assign is a statement that assigns a value to a variable
type Assign struct {
	targ ID
	val  Expr
}

// Execute applies an Assign statement to the passed Environment,
// It returns the environment, with the Assignment applied
func (a Assign) Execute(env Environment) (Environment, error) {
	realVal, err := a.val.EvalExpression(env)
	if err != nil {
		return env, err
	}
	err = env.Set(a.targ, realVal)
	return env, err
}
