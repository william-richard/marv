package marv

import (
	"fmt"
	"strconv"
)

// ID is a variable identifier
type ID struct {
	name string
}

// EvalExpression looks up the ID from the Environment
func (i ID) EvalExpression(env Environment) (t MValue, err error) {
	return env.Lookup(i)
}

// IntExpr is an integer expression
// It stores the value of the integer as a string
type IntExpr struct {
	intCap string
}

// EvalExpression evaluates the value stored in the IntExpr, given the other
// variables in the Environment, and returns an integer MValue
func (i IntExpr) EvalExpression(env Environment) (MValue, error) {
	var res MValue
	_, err := strconv.Atoi(i.intCap)
	if err != nil {
		return res, err
	}
	return MValue{Int, i.intCap}, nil
}

// Plus is a simple addition expression
type Plus struct {
	a Expr
	b Expr
}

// EvalExpression evaluates the Plus expression, given the variable values
// in the Environment.  Returns the resulting sum
func (p Plus) EvalExpression(env Environment) (MValue, error) {
	var res MValue
	aVal, err := p.a.EvalExpression(env)
	if err != nil {
		return res, err
	}
	bVal, err := p.b.EvalExpression(env)
	if err != nil {
		return res, err
	}

	if aVal.mtype != bVal.mtype {
		return res, fmt.Errorf("Types of values passed to plus are different! Got %s with value %s and %s with value %s", aVal.mtype, aVal.contents, bVal.mtype, bVal.contents)
	}

	switch aVal.mtype {
	case Int:
		aInt, err := strconv.Atoi(aVal.contents)
		if err != nil {
			return res, err
		}

		bInt, err := strconv.Atoi(bVal.contents)
		if err != nil {
			return res, err
		}

		return MValue{Int, strconv.Itoa(aInt + bInt)}, nil
	case String:
		return MValue{String, aVal.contents + bVal.contents}, nil
	}
	return res, fmt.Errorf("Expected int or string for plus. Got %#v and %#v", aVal, bVal)
}

// StringExpr is simply a string expression
// right now, it's just a raw string
type StringExpr struct {
	strCap string
}

// EvalExpression evaluates the StringExperssion, given the state of the
// Environment and returns the resulting MValue
// Right now, it just returns the string, but eventually it should do
// variable interpolation in the string
func (s StringExpr) EvalExpression(env Environment) (res MValue, err error) {
	// TODO string interpolation?
	return MValue{String, s.strCap}, nil
}
