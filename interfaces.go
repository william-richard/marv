package marv

// Smt is a stement
// statements have an effect or side effect
// like an assignment of a variable to a value
// or printing a string
type Smt interface {
	// execute the statement, returning the modified environment or an error
	Execute(env Environment) (Environment, error)
}

// Expr is a type holding an expression
// expressions evaluate to values
// i.e. they have results
// such as literal values
// or simple equations
type Expr interface {
	// evaluate the expression, returning the result
	EvalExpression(env Environment) (MValue, error)
}

//go:generate stringer -type=MType

// MType represents basic literals
// like int, bool, string, etc
type MType int

// MType constants
// simply add to this list and run `go generate' to create a new type
const (
	Int MType = iota
	Boolean
	String
)

// MValue is a value
// every value has exactly one type
// as well as the contents, in a string
// the string will be converted to the correct golang
// type based on the the Type of the value
type MValue struct {
	mtype    MType
	contents string
}
