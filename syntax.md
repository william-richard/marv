# Syntax for Marv

# Expressions

**Expressions evaluate to values!**

Expressions should all have an `EvalExpression` function, which returns the value of the expression.

## Literals

### Int

Just a numeral - `5`, `2`

#### Basic Math

`5 + 2`

`5 - 2`

`5 * 2`

`5 / 2`

`5 % 2`

### String

Single or double quoted array of chars

`'hello'`

`"world"`

Should there be a difference between single and double quote? 

How do variables get inserted into strings?

`u = "variable = ${variable}"`

Should `eval_expression` should evaluate any variables in the string, and return the literal value of it?

### Boolean
`true`

`false`

#### Conditional operations

`5 == 5`

`true and true`

`false or false`

String equality?  Probably best to compute by the value of the string.

# Statements

**Statements do things / have side effects!**

All statements should have an `Execute` function, which executes the statement.

## Assignment

Literal types should be implied.

We care about spaces! Must be exactly 1 space between variables/values and operators.

`a = 5`

`s = 'hello'`

`t = true`

## Print

`print a`

`print <expression>`
