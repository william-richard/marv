package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/william-richard/marv"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Please provide a file to interpret")
	}

	fileBytes, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	lines, err := marv.Preprocess(fileBytes)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Lines:\n%#v\n", lines)

	p := marv.NewParser()

	smts, err := p.Parse(lines)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Parsed:\n%#v\n", smts)

	e := marv.NewEnvironment()

	err = e.ExecuteStatements(smts)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Finished execution\n")
	fmt.Println("Final Env:")
	e.PrintEnv()
}
