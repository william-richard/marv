package marv

import (
	"reflect"
	"testing"
)

func GenericParseTest(t *testing.T, in []Line, want []Smt) {

	p := NewParser()

	got, err := p.Parse(in)

	if err != nil {
		t.Errorf("Parse(%v) threw an error. %v", in, err.Error())
	}
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Parse(%#v) == %#v. Wanted %#v", in, got, want)
	}
}

func TestParseInt(t *testing.T) {

	cases := []struct {
		in   []Line
		want []Smt
	}{
		{
			[]Line{Line{"a = 5"}}, []Smt{Assign{ID{"a"}, IntExpr{"5"}}},
		},
		{
			[]Line{Line{"a = 4 + 3"}},
			[]Smt{Assign{ID{"a"}, Plus{IntExpr{"4"}, IntExpr{"3"}}}},
		},
		{
			[]Line{
				Line{"first = 5"},
				Line{"second = 4"},
				Line{"res = first + second"},
			},
			[]Smt{
				Assign{ID{"first"}, IntExpr{"5"}},
				Assign{ID{"second"}, IntExpr{"4"}},
				Assign{ID{"res"}, Plus{ID{"first"}, ID{"second"}}},
			},
		},
		{
			[]Line{Line{`plusInt = 5 + 2 + 3`}},
			[]Smt{Assign{
				ID{"plusInt"},
				Plus{IntExpr{"5"}, Plus{IntExpr{"2"}, IntExpr{"3"}}}}},
		},
	}

	for _, c := range cases {
		GenericParseTest(t, c.in, c.want)
	}

}

func TestParseString(t *testing.T) {
	cases := []struct {
		in   []Line
		want []Smt
	}{
		{
			[]Line{Line{`s = "hello world"`}},
			[]Smt{Assign{ID{"s"}, StringExpr{"hello world"}}},
		},
		{
			[]Line{Line{`fiveStr = "5"`}},
			[]Smt{Assign{ID{"fiveStr"}, StringExpr{"5"}}},
		},
		{
			[]Line{Line{`plusStr = "5 + 2"`}},
			[]Smt{Assign{ID{"plusStr"}, StringExpr{"5 + 2"}}},
		},
		{
			[]Line{Line{`concatStr = "hello" + "world"`}},
			[]Smt{Assign{ID{"concatStr"}, Plus{StringExpr{"hello"}, StringExpr{"world"}}}},
		},
		{
			[]Line{Line{`concatStr = "hello" + " " + "world"`}},
			[]Smt{
				Assign{
					ID{"concatStr"},
					Plus{
						StringExpr{"hello"},
						Plus{
							StringExpr{" "},
							StringExpr{"world"},
						},
					},
				},
			},
		},
	}

	for _, c := range cases {
		GenericParseTest(t, c.in, c.want)
	}
}

func BenchmarkNewParse(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewParser()
	}
}

func BenchmarkParseAssignInt(b *testing.B) {
	p := NewParser()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := p.Parse([]Line{Line{"A = 5"}})
		if err != nil {
			b.Errorf("Parsing failed! %s", err.Error())
		}
	}
}

func BenchmarkParseAssignStr(b *testing.B) {
	p := NewParser()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := p.Parse([]Line{Line{`s = "hello world"`}})
		if err != nil {
			b.Errorf("Parsing failed! %s", err.Error())
		}
	}
}

func BenchmarkParseAssignPlusInt(b *testing.B) {
	p := NewParser()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := p.Parse([]Line{Line{"b = 2 + 3"}})
		if err != nil {
			b.Errorf("Parsing failed! %s", err.Error())
		}
	}
}

func BenchmarkParseAssignPlusStr(b *testing.B) {
	p := NewParser()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := p.Parse([]Line{Line{`s = "hello" + "world"`}})
		if err != nil {
			b.Errorf("Parsing failed! %s", err.Error())
		}
	}
}
